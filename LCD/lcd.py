import serial
ser = serial.Serial(port='/dev/ttyAMA0',baudrate=9600,parity=serial.PARITY_NONE,bytesize=serial.EIGHTBITS,stopbits=serial.STOPBITS_ONE)


CLEAR = chr(12)
BIG_CHAR = chr(2)
SMALL_CHAR = chr(3)
HIDE_CURSOR = chr(4)
UNDERLINE_CURSOR = chr(5)
BLINKING_BLOCK_CURSOR = chr(6)
BACKSPACE = chr(8)
TAB = chr(9)
NEWLINE = chr(10)
UPLINE = chr(11)
BACKLIGHT_ON = chr(14)
BACKLIGHT_OFF = chr(15)

ser.write(CLEAR)
ser.write(HIDE_CURSOR)
ser.write(BIG_CHAR)
ser.write("YES")