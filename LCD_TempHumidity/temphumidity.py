#!/usr/bin/python

import subprocess
import re
import sys
import time
import datetime
import serial

# LCD Constants
HOME = chr(1)
BIG_CHAR = chr(2)
SMALL_CHAR = chr(3)
HIDE_CURSOR = chr(4)
UNDERLINE_CURSOR = chr(5)
BLINKING_BLOCK_CURSOR = chr(6)
BACKSPACE = chr(8)
TAB = chr(9)
NEWLINE = chr(10)
UPLINE = chr(11)
CLEAR = chr(12)
BACKLIGHT_ON = chr(14)
BACKLIGHT_OFF = chr(15)

# Start Serial Port
ser = serial.Serial(port='/dev/ttyAMA0',baudrate=9600,parity=serial.PARITY_NONE,bytesize=serial.EIGHTBITS,stopbits=serial.STOPBITS_ONE)



ser.write(CLEAR)
ser.write(HIDE_CURSOR)
ser.write(BIG_CHAR)
ser.write(BACKLIGHT_ON)

# Continuously append data
while(True):
  # Run the DHT program to get the humidity and temperature readings!

  output = subprocess.check_output(["./Adafruit_DHT", "2302", "4"]);
  print output
  matches = re.search("Temp =\s+([0-9.]+)", output)
  if (not matches):
	time.sleep(3)
	continue
  temp = float(matches.group(1)) * 1.8 +32
  
  # search for humidity printout
  matches = re.search("Hum =\s+([0-9.]+)", output)
  if (not matches):
	time.sleep(3)
	continue
  humidity = float(matches.group(1))

  print "Temperature: %.1f C" % temp
  print "Humidity:    %.1f %%" % humidity
 
  # write to LCD
  ser.write(CLEAR)
  ser.write(BIG_CHAR)
  ser.write("TEMP")
  time.sleep(2)
  
  ser.write(CLEAR)
  ser.write(BIG_CHAR)
  ser.write("%.1f" % temp)
  time.sleep(2)
  
  ser.write(CLEAR)
  ser.write(BIG_CHAR)
  ser.write("HUM")
  time.sleep(2)
  
  ser.write(CLEAR)
  ser.write(BIG_CHAR)
  ser.write("%.1f" % humidity)
  time.sleep(2)
