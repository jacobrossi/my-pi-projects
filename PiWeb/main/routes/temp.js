var url = require("url");
var exec = require("child_process").exec;
var tryCount = 0;
/*
 * GET temp
 */

exports.tempsense = function(req, res){
    fetchData(res);
}

function fetchData(res) {
    if (tryCount++ > 5) {
        res.json({temp: tryCount, humidity: 42.1});
        return;
    }
    exec("sudo /usr/share/adafruit/webide/repositories/Adafruit-Raspberry-Pi-Python-Code/Adafruit_DHT_Driver/Adafruit_DHT 22 4", function (error, stdout, stderr) {
        console.log(error + "\n" + stdout + "\n" + stderr);
        var matches = stdout.match(/Temp =\s+([0-9.]+)/);
        if(!matches || matches.length<2) {
            setTimeout(fetchData,2000,res);
            return;
        }
        temp = Math.round((matches[1] * 1.8 + 32) * 10) / 10; //Convert C to F and round to nearest 10th
        
        matches = stdout.match(/Hum =\s+([0-9.]+)/);
        if(!matches || matches.length<2) {
            setTimeout(fetchData,2000,res);
            return;
        }
        humidity = Math.round(matches[1] * 10) / 10; //Round to nearest 10th
            
        res.json({temp: temp, humidity: humidity});
    });  
}
