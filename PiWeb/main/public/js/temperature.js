var xhr = new XMLHttpRequest();
xhr.onload = updateTemp;
xhr.responsType = "json";
xhr.open("GET", "/temp")
xhr.send();



function updateTemp() {
    var result = JSON.parse(xhr.response);
    var temp = document.getElementById("temp_meter");
    var humidity = document.getElementById("humidity_meter");
    var temp_text = document.getElementById("temp_text");
    var humidity_text = document.getElementById("humidity_text");
    temp.max = 85;
    temp.min = 55;
    temp.value = result.temp;
    temp_text.innerHTML = result.temp + "&deg;";
    
    humidity.max = 100;
    humidity.min = 0;
    humidity.value = result.humidity;
    humidity_text.innerHTML = result.humidity + "%";
}