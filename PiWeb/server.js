var http = require("http");
var url = require("url");
var exec = require("child_process").exec;


function start(route) {
  function onRequest(request, response) {
    var pathname = url.parse(request.url).pathname;
    console.log("Request for " + pathname + " received.");

    route(pathname);

    
    exec("sudo /home/pi/Adafruit-Raspberry-Pi-Python-Code/Adafruit_DHT_Driver/Adafruit_DHT 2302 4", function (error, stdout, stderr) {
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("<!doctype html><html><head><meta http-equiv='viewport' content='width=device-width'></head><body>")
        response.write(stdout);
        response.end();
    });
    //response.write("foo");
    
  }

  http.createServer(onRequest).listen(8888);
  console.log("Server has started.");
}

exports.start = start;